var express = require('express');
var app = express();
var os = require('os');

var PORT = process.env.PORT || 8080;

app.get('/', function(req, res) {
	res.send("Hey there! This is running on: <u><b>" + os.hostname() + "</b></u>.");
});

app.listen(PORT, function() {
	console.log('Listening on port', PORT);
});
