# Use the node:alpine base image which is smaller than the default node.js image.
FROM node:alpine

# As a security provision create the 'disrupt' user/group to run the app instead of root.
RUN addgroup -S disrupt && adduser -S -G disrupt disrupt

# Set current working directory inside the container.
WORKDIR /usr/src/app

# Copy and install package.json which contains the application dependencies.
COPY package.json .
RUN npm install --production

#Copy the rest of the files and verify file permissions.
COPY . .
RUN chown -R disrupt:disrupt /usr/src/app

# Set working user as 'disrupt'.
USER disrupt

# Allow the app to be accessable from the outside world at port 8080.
EXPOSE 8080

# Run the start script defined in package.json which just starts the app.
CMD [ "npm", "start" ]
